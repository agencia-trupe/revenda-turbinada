<?php

use Illuminate\Database\Seeder;

class BonusTipo extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bonus_tipo')->insert([
            'nome' => 'revenda',
        ]);

        DB::table('bonus_tipo')->insert([
            'nome' => 'linha nova',
        ]);
    }
}
