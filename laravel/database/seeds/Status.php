<?php

use Illuminate\Database\Seeder;

class Status extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status')->insert([
            'nome' => 'aprovado',
        ]);
        DB::table('status')->insert([
            'nome' => 'reprovado',
        ]);
        DB::table('status')->insert([
            'nome' => 'aguardando validacao',
        ]);
    }
}
