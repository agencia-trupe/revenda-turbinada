<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBonusUtilizadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonus_utilizado', function(Blueprint $table){
            $table->increments('id');
            $table->string('numero_nf')->nullable();
            $table->string('data')->nullable();
            $table->string('valor')->nullable();
            $table->timestamps();

            $table->integer('usuario_id')->unsigned();
            $table->foreign('usuario_id')->references('id')->on('cadastros');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bonus_utilizado');
    }
}
