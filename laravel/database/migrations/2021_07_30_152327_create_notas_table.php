<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notas', function(Blueprint $table){
            $table->increments('id');
            $table->integer('usuario_id')->unsigned();            
            $table->string('cnpj_terceiro')->nullable();
            $table->string('tipo_bonus')->nullable();
            $table->string('razao_social')->nullable();
            $table->string('numero_nf')->nullable();
            $table->string('anexo_nf')->nullable();
            $table->string('valor')->nullable();
            $table->string('status')->nullable();
            $table->date('status_data');
            $table->string('bonus')->nullable();
            $table->timestamps();

            $table->foreign('usuario_id')->references('id')->on('cadastros');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notas');
    }
}
