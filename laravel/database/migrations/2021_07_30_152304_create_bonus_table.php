<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBonusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonus', function(Blueprint $table){
            $table->increments('id');
            $table->integer('usuario_id')->unsigned();
            $table->string('total')->nullable();
            $table->string('total_bonus_revenda')->nullable();
            $table->string('total_bonus_linha_nova')->nullable();
            $table->string('total_bonus')->nullable();
            $table->string('total_bonus_utilizado')->nullable();
            $table->timestamps();
            
            $table->foreign('usuario_id')->references('id')->on('cadastros');
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bonus');
    }
}
