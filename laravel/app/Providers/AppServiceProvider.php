<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($view) {
            $view->with('config', \App\Models\Configuracoes::first());
            if(\Auth::guard('cadastros')->check()){
                $view->with('bonus', \App\Models\Bonus::where('usuario_id', '=', \Auth::guard('cadastros')->user()->id)->first());
                $view->with('saldo', \App\Models\BonusUtilizado::where('usuario_id', '=', \Auth::guard('cadastros')->user()->id)->first());
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
