<?php

namespace App\Helpers;
use App\Models\Notas;
use App\Models\BonusTipo;
use App\Models\Bonus;
use App\Models\BonusUtilizado;

class Tools
{
    public static function routeIs($routeNames)
    {
        foreach ((array) $routeNames as $routeName) {
            if (str_is($routeName, \Route::currentRouteName())) {
                return true;
            }
        }

        return false;
    }

    public static function fileUpload($file, $path)
    {
        $file_name = "";
        $file->move(public_path('assets/img/'.$path), time() . '-' . $file->getClientOriginalName());
        $file_name = time() .'-'. $file->getClientOriginalName();

        return $file_name;
    }

    public static function somaRevenda()
    {
        return self::calculaBonus(BonusTipo::REVENDA, 'total_bonus_revenda');
    }

    public static function somaLinhaNova()
    {
        return self::calculaBonus(BonusTipo::LINHA_NOVA, 'total_bonus_linha_nova');
    }

    public static function calculaBonusUtilizado()
    {
        try{
            $valores = BonusUtilizado::select('valor')
            ->where('usuario_id', '=', \Auth::guard('cadastros')->user()->id)
            ->get();
            
            $total = self::formataValores($valores, 'valor');            
            Bonus::where('usuario_id','=',\Auth::guard('cadastros')->user()->id)->update(['total_bonus_utilizado' => $total]);
    
            return true;
        }catch(\Exception $e){
            return back()->withErrors(['Erro ao calcular bonus utilizado:' .$e->getMessage()]);
        }
    }

    public static function descontaBonusUtilizado($cliente_id){
        try{
            $bonus = Bonus::select('total_bonus')
            ->where('usuario_id', '=', $cliente_id)
            ->get();

            $bonus_utilizado = Bonus::select('total_bonus_utilizado')
            ->where('usuario_id', '=', $cliente_id)
            ->get();
            
            $bonus = self::formataValores($bonus, 'total_bonus');
            $bonus_utilizado = self::formataValores($bonus_utilizado, 'total_bonus_utilizado');
            
            $total = str_replace('.',',', str_replace(',','.',$bonus)) - str_replace(',','.',str_replace(',','.',$bonus_utilizado));
            
            BonusUtilizado::where('usuario_id','=',$cliente_id)->update(['saldo' => $total]);
    
            return true;
        }catch(\Exception $e){
            return back()->withErrors(['Erro ao atualizado total com bonus utilizado:' .$e->getMessage()]);
        }
    }

    public static function recalculaBonusUtilizado($cliente_id){
        try{
            $valores = BonusUtilizado::select('valor')
            ->where('usuario_id', '=', $cliente_id)
            ->get();
            
            $total = self::formataValores($valores, 'valor');            
            Bonus::where('usuario_id','=',$cliente_id)->update(['total_bonus_utilizado' => $total]);
    
            return true;
        }catch(\Exception $e){
            return back()->withErrors(['Erro ao calcular bonus utilizado:' .$e->getMessage()]);
        }
    }
    
    
    public static function somaTotalBonus()
    {
        try{
            $bonus = Notas::select('bonus')
            ->where('usuario_id', '=', \Auth::guard('cadastros')->user()->id)
            ->get();    

            $total = self::formataValores($bonus, 'bonus');

            Bonus::where('usuario_id','=',\Auth::guard('cadastros')->user()->id)->update(['total_bonus' => $total]);
    
            return true;
        }catch(\Exception $e){
            return back()->withErrors(['Erro ao somar total bonus nota:' .$e->getMessage()]);
        }
    }

    public static function recalculaTotalBonus($usuario_id)
    {
        try{
            $bonus = Notas::select('bonus')
            ->where('usuario_id', '=', $usuario_id)
            ->get();    

            $total = self::formataValores($bonus, 'bonus');

            Bonus::where('usuario_id','=',$usuario_id)->update(['total_bonus' => $total]);
    
            return true;
        }catch(\Exception $e){
            return back()->withErrors(['Erro ao somar total bonus nota:' .$e->getMessage()]);
        }
    }

    

    private static function calculaBonus($tipo_bonus, $total_bonus)
    {
        try{
            $notas = Notas::select('bonus')
            ->where('tipo_bonus', '=', $tipo_bonus)
            ->where('usuario_id', '=', \Auth::guard('cadastros')->user()->id)
            ->get();
    
            $total = self::formataValores($notas, 'bonus');
    
            if(empty(Bonus::where('usuario_id','=',\Auth::guard('cadastros')->user()->id)->get()->toArray())){
                Bonus::create(['usuario_id' => \Auth::guard('cadastros')->user()->id, $total_bonus => $total]);
            }else{
                Bonus::where('usuario_id','=',\Auth::guard('cadastros')->user()->id)->update([$total_bonus => $total]);
            }
                
            return true;
        }catch(\Exception $e){
            return back()->withErrors(['Erro ao calcular bonus nota:' .$e->getMessage()]);
        }
    }

    public static function recalculaBonus($tipo_bonus, $total_bonus, $usuario_id)
    {
        try{
            $notas = Notas::select('bonus')
            ->where('tipo_bonus', '=', $tipo_bonus)
            ->where('usuario_id', '=', $usuario_id)
            ->get();
            
            $total = self::formataValores($notas, 'bonus');
    
            Bonus::where('usuario_id','=',$usuario_id)->update([$total_bonus => $total]);
                
            return true;
        }catch(\Exception $e){
            return back()->withErrors(['Erro ao calcular bonus nota:' .$e->getMessage()]);
        }
    }


    private static function formataValores($valores, $nome_campo)
    {
        $total = [];
        foreach($valores as $v){
            $valor = str_replace('.','',$v[$nome_campo]);
            array_push($total, str_replace(',','.',$valor));
        }

        return number_format(array_sum($total), 2, ',', '.');
    }

}
