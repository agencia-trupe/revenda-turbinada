<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notas extends Model
{
    protected $table = 'notas';

    protected $guarded = ['id'];

    protected $visible = [
        'id', 
        'usuario_id',
        'cnpj_terceiro',
        'tipo_bonus',
        'razao_social',
        'numero_nf',
        'anexo_nf',
        'valor',
        'status',
    ];

    public function cadastros()
    {
        return $this->belongsTo(\App\Models\Cadastros::class, 'usuario_id');
    }
    
}
