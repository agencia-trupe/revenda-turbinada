<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BonusTipo extends Model
{
    protected $table = 'bonus_tipo';

    protected $guarded = ['id'];

    const REVENDA = 1;
    const LINHA_NOVA = 2;

    public static function getNameType($id)
    {
        $tipo_nome = BonusTipo::find($id);
        return $tipo_nome->nome;
    }

}
