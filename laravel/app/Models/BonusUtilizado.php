<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BonusUtilizado extends Model
{
    protected $table = 'bonus_utilizado';

    protected $guarded = ['id'];

    public function cadastros()
    {
        return $this->belongsTo(\App\Models\Cadastros::class, 'usuario_id', 'id');
    }
}
