<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfNotAdmin
{
    public function handle($request, Closure $next)
    {
        /*if (auth()->user()->tipo !== 'administracao' || auth()->user()->tipo !== 'gerente_automocao' || auth()->user()->tipo !== 'gerente_componentes') {
            return redirect()->route('painel');
        }*/

        return $next($request);
    }
}
