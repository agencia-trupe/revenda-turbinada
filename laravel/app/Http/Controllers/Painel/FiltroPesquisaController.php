<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Notas;
use App\Models\BonusUtilizado;
use App\Models\BonusTipo;
use App\Helpers\Tools;

class FiltroPesquisaController extends Controller
{
    public function notas(Request $request)
    {
        $pesquisa_registro = Notas::where('numero_nf', '=', $request->get('numero_nf'))
            ->orWhere('razao_social', '=', $request->get('razao_social'))
            ->get() ?: 'Nenhuma nota encontrada.';

        return view($request->get('rota'), compact('pesquisa_registro'));
    }

    public function bonus(Request $request)
    {
        $pesquisa_registro = collect(BonusUtilizado::all())->filter(function($item) use ($request){
            return $item->cadastros->cnpj == $request->get('cnpj');
        });

        return view($request->get('rota'), compact('pesquisa_registro'));
    }

    public function atualizaSaldoBonus($id)
    {
        //atualiza saldo LINHA NOVA
        Tools::recalculaBonus(BonusTipo::LINHA_NOVA, 'total_bonus_linha_nova', $id);
        //atualiza saldo REVENDA
        Tools::recalculaBonus(BonusTipo::REVENDA, 'total_bonus_revenda', $id);
        //atualiza total bonus
        Tools::recalculaTotalBonus($id);
        //recalcula bonus utilizado
        Tools::recalculaBonusUtilizado($id);
        //desconta o total bonus sobre o bonus utilizado
        Tools::descontaBonusUtilizado($id);
        
        return redirect()->route('painel.bonus.index')->with('success', 'Saldo atualizado');
    }
}
