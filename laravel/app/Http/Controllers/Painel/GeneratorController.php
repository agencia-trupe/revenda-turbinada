<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Artisan;

class GeneratorController extends Controller
{
    public function index()
    {
        return view('painel.generator.index');
    }

    public function submit(Request $request)
    {
        $this->validate($request, [
            'type'         => 'required',
            'resourceName' => 'required',
            'unitName'     => 'required_if:type,default'
        ]);

        $type = $request->get('type');

        $parameters = [
            'resourceName' => $request->get('resourceName'),
            'fields'       => $request->get('fields'),
        ];

        if(!empty($request->get('campos_nome')[0])) {
            $fields = '';
            foreach ($request->get('campos_nome') as $key => $campo_nome) {
                $field = [
                    $campo_nome,
                    $request->get('campos_tipo')[$key],
                    $request->get('campos_validation')[$key]
                ];

                $field = implode(':', $field);

                if (array_key_exists($key+1, $request->get('campos_nome'))) {
                    $fields .= $field.',';
                } else {
                    $fields .= $field;
                }
            }

            if (!empty($request->get('fields'))) {
                $parameters['fields'] = $request->get('fields').','.$fields;
            } else {
                $parameters['fields'] = $fields;
            }
        }

        if ($type == 'default') {
            $parameters['unitName'] = $request->get('unitName');
            $parameters['--slug']   = $request->get('slug');

            if ($request->get('paginate')) {
                $parameters['--paginate'] = $request->get('paginate');
            }
            if ($request->get('sortable')) {
                $parameters['--sortable'] = true;
            }
            if ($request->get('gallery')) {
                $parameters['--gallery'] = true;
            }
            if ($request->get('categories')) {
                $parameters['--categories'] = true;
            }
            if ($request->get('tags')) {
                $parameters['--tags'] = true;
            }
        }

        if ($request->get('force')) {
            $parameters['--force'] = true;
        }

        try {

            Artisan::call('resource:'.$type, $parameters);
            $output = Artisan::output();

            return back()->with(compact('output'))->withInput();

        } catch (\Exception $e) {

            return back()->withErrors([$e->getMessage()])->withInput();

        }

    }
}
