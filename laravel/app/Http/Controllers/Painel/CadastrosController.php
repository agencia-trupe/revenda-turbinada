<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CadastrosRequest;
use App\Http\Controllers\Controller;
use App\Models\Cadastros;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class CadastrosController extends Controller
{
    public function index()
    {   
        if(\Auth::user()->tipo == "administracao"){
            $usuarios = Cadastros::all();
        }else{
            $usuarios = Cadastros::where('gerente_automocao_email', '=', \Auth::user()->email)
            ->orWhere('gerente_componentes_email', '=', \Auth::user()->email)
            ->get();                      
        }
        return view('painel.cadastros.index', compact('usuarios'));
    }

    public function create()
    {
        return view('painel.cadastros.create');
    }

    public function store(CadastrosRequest $request, Cadastros $registro){

        try{
            //valida de gerente email está vazio
            if(!$request->has('gerente_automocao_email') && !$request->has('gerente_componentes_email')){
                return back()->withErrors(['Preencha pelo menos um campo Gerente']);
            }

            $input = $request->all();            
            $usuario = $registro->create($input);


            //envia email ao usuario, informando os dados de acesso
            $this->sendMailUserCreated($usuario);

            return redirect()->route('painel.cadastros.index')->with('success', 'Registro criado com sucesso.');
        }catch (\Exception $e) {
            return back()->withErrors(['Erro ao inserir registro: '.$e->getMessage()]);
        }
    }

    public function reenviaEmailUsuario($id)
    {
        $usuario = Cadastros::find($id);
        $this->sendMailUserCreated($usuario);
        return redirect()->route('painel.cadastros.index')->with('success', 'Confirmação de conta reenviada ao usuário.');
    }

    public function edit(Cadastros $registro, $id)
    {
        $usuario = $registro::find($id);
        return view('painel.cadastros.edit', compact('usuario'));
    }

    public function update(CadastrosRequest $request, Cadastros $registro, $id){
        try{
            $registro->find($id)->update($request->all());

            return redirect()->route('painel.cadastros.index')->with('success', 'Registro atualizado com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Erro ao atualizar registro:'. $e->getMessage()]);
        }
    }

    private function sendMailUserCreated($usuario)
    { 
         Mail::send('emails.usuario-criado', ['usuario' => $usuario], function($m) use ($usuario)
         {
             $m->to($usuario->email, config('app.name'))
                ->subject('[ACESSO CRIADO] '.config('app.name'));
         });
    }

    public function destroy(Cadastros $registro, $id)
    {
        $registro->find($id)->delete();
        return redirect()->route('painel.cadastros.index')->with('success', 'Registro deletado com sucesso.');

    }
}
