<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\BonusUtilizado;
use App\Models\Bonus;
use App\Models\Cadastros;
use App\Models\User;
use App\Helpers\Tools;

class BonusController extends Controller
{
    public function index()
    {
        $bonus_utilizado = BonusUtilizado::select('*')->orderBy('data', 'desc')->get();
        return view('painel.bonus.index', compact('bonus_utilizado'));
    }

    public function edit(BonusUtilizado $bonus_utilizado, $id)
    {
        $usuario = Cadastros::find($id);
        return view('painel.bonus.edit', compact('usuario'));
    }

    public function update(Request $request, BonusUtilizado $registro, $id)
    {
        try{
            $input = $request->only(['usuario_id', 'valor_bonus', 'numero_nf', 'data']);
            $input['valor'] = $input['valor_bonus'];

            //format date
            $parts = explode('-', $input['data']);
            $date  = $parts[2].'-'.$parts[1].'-'.$parts[0];
            $input['data'] = $date;

            unset($input['valor_bonus']);
            $usuario = $registro->create($input);
            
            //atualiza saldo cliente
            Tools::descontaBonusUtilizado($usuario->usuario_id);
            
            return redirect()->route('painel.bonus.index')->with('sucesso', 'Bônus utilizado adicionado ao usuário.');
        }catch(\Exception $e){
            return back()->withErrors(['Erro ao atualizar bônus utilizado: ' . $e->getMessage()]);
        }
    }

    public function destroy(BonusUtilizado $registro, $id)
    {
        try{
            $registro->find($id)->delete();
            return redirect()->route('painel.bonus.index')->with('sucesso', 'Bônus utilizado deletado.');
        }catch(\Excepion $e){
            return back()->withErrors(['Erro ao deletar nota com bônus utilizado: ' . $e->getMessage()]);
        }
    }
}
