<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Notas;

class NotasController extends Controller
{
    public function index()
    {
        if(\Auth::user()->tipo == "gerente_componentes"){
            $notas = collect(Notas::select('*')->orderBy('created_at', 'desc')->get())->filter(function ($item, $key){
                return $item->cadastros->gerente_componentes_email == \Auth::user()->email;
            });            
        }else if(\Auth::user()->tipo == "gerente_automocao"){
            $notas = collect(Notas::select('*')->orderBy('created_at', 'desc')->get())->filter(function ($item, $key){
                return $item->cadastros->gerente_automocao_email == \Auth::user()->email;
            });
        }else{
            $notas = Notas::select('*')->orderBy('created_at', 'desc')->get();
        }
        return view('painel.notas.index', compact('notas'));
    }

    public function edit($id)
    {
        $nota = Notas::where('id',$id)->first();
        return view('painel.notas.edit', compact('nota'));
    }

    public function update(Request $request, Notas $registro, $id)
    {
        try{
            $input = $request->all();
            $input['status_data'] = date('Y-m-d H:i:s');
            $registro->find($id)->update($input);
            return redirect()->route('painel.notas.index')->with('success', 'Nota atualizada com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Erro ao atualizar nota:' .$e->getMessage()]);
        }
    }

    public function destroy(Notas $registro, $id)
    {
        try{
            $registro->find($id)->delete();
            return redirect()->route('painel.notas.index')->with('success', 'Nota removida com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Erro ao deletar nota: '. $e->getMessage()]);
        }
    }
}
