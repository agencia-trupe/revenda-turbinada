<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BonusUtilizado;
use App\Http\Requests;
use App\Helpers\Tools;

class ExtratoBonusController extends Controller
{
    public function index()
    {
        //atualiza total bonus utilizado pelo usuario
        Tools::calculaBonusUtilizado();

        $registros = BonusUtilizado::where('usuario_id', '=', \Auth::guard('cadastros')->user()->id)->with('cadastros')->get();
        return view('frontend.extrato-de-bonus', compact('registros'));
    }
}
