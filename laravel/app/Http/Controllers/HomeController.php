<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\AceiteDeCookies;
use Illuminate\Http\Request;
use App\Models\PoliticaDePrivacidade;
use App\Models\Cadastros;
use App\Helpers\Tools;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $verificacao = AceiteDeCookies::where('ip', $request->ip())->first();
        if(Auth::guard('cadastros')->check()){
            return view('frontend.notas-cadastradas', compact('verificacao'));
        }
        return view('frontend.login', compact('verificacao'));
    }

    public function login(Request $request)
    { 
        if(Auth::guard('cadastros')->check()){
            return view('frontend.notas-cadastradas');
        }else{
            return view('frontend.login');
        }
    }

    public function esqueci(){
        return view('frontend.esqueci');
    }

    public function postCookies(Request $request)
    {
        $input = $request->all();
        $input['ip'] = $request->ip();

        AceiteDeCookies::create($input);

        return redirect('/');
    }

    public function politicaDePrivacidade()
    {
        $registro = PoliticaDePrivacidade::first();
        return view('frontend.politica-de-privacidade', compact('registro'));
    }

}
