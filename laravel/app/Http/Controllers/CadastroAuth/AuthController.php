<?php

namespace App\Http\Controllers\CadastroAuth;

use App\Models\Cadastros;
use Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use App\Http\Requests\LoginCadastrosRequest;
use App\Http\Requests\CriarSenhaCadastrosRequest;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';
    protected $guard      = 'cadastros';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest', ['except' => 'logout']);
    }

    public function logout()
    {
        Auth::guard('cadastros')->logout();
        return redirect('/');
    }

    protected function login(LoginCadastrosRequest $request) 
    {
        $usuario = Cadastros::where('cnpj', $request->cnpj)->first();

        if (!$usuario) {
            return redirect()->route('login-cadastro')->withErrors(['Registro não encontrado.']);
        }

        if (empty($usuario->senha)){
            $cnpj = $usuario->cnpj;
            return view('frontend.login', compact('cnpj'));
        }

        if (Hash::check($request->senha, $usuario->senha)) {
            Auth::guard('cadastros')->login($usuario);
            return redirect()->route('notas-cadastradas');
        }

        return redirect()
            ->route('login-cadastro')
            ->withInput($request->only('email'))
            ->withErrors(['email' => 'CNPJ ou Senha incorreto.']);
    }

    protected function createPassword(CriarSenhaCadastrosRequest $request, Cadastros $registro){
        try{            
            $senha = bcrypt($request->get('senha'));
            $registro->where('cnpj', $request->get('cnpj'))->update(['senha' => $senha]);

            return redirect()->route('login-cadastro')->with('success', 'Senha criada com sucesso, faça o login a baixo.');
        }catch(\Exception $e){
            return back()->withErrors(['Erro ao criar senha:' .$e->getMessage()]);
        }
    }

    public function showLoginForm()
    {
        if (Auth::guard('cadastros')->check()) {
            return redirect()->route('notas-cadastradas');
        }

        return view('frontend.login');
    }
}
