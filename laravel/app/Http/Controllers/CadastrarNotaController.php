<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Notas;
use App\Models\BonusTipo;
use App\Http\Requests\CadastrarNotaRequest;
use App\Helpers\Tools;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class CadastrarNotaController extends Controller
{
    public function index()
    {   
        //atualiza total de bonus
        Tools::somaTotalBonus();

        //atualiza notas de revenda
        Tools::somaRevenda();

        //atualiza notas de linha nova
        Tools::somaLinhaNova();

        //atualiza total bonus utilizado pelo usuario
        Tools::calculaBonusUtilizado();
        
        $registros = Notas::where('usuario_id', '=', \Auth::guard('cadastros')->user()->id)->get();
        return view('frontend.notas-cadastradas', compact('registros'));
    }

    public function cadastrarNota()
    {
        return view('frontend.cadastrar-nota');
    }

    public function cadastrarRevenda()
    {
        return view('frontend.revenda.create');
    }

    public function revenda(CadastrarNotaRequest $request, Notas $registro)
    {
        try{
            $input = $request->all();
            $input['tipo_bonus'] = BonusTipo::REVENDA;
            if(!empty($input['anexo_nf'])) $input['anexo_nf'] = Tools::fileUpload($input['anexo_nf'], 'revenda/');
            $nova_nota = $registro->create($input);            

            //notifica gerente da nova criada
            $usuario = $nova_nota->cadastros->first();
            $gerente = User::where('email', '=', $usuario->gerente_automocao_email)
            ->orWhere('email', '=', $usuario->gerente_componentes_email)
            ->first();
            
            $this->enviaEmailGerente($gerente, $usuario, 'Revenda');

            return redirect()->route('notas-cadastradas')->with('success', 'Nota revenda inserida com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Erro ao registrar nota:' .$e->getMessage()]);
        }
    }

    public function cadastrarLinhaNova()
    {
        return view('frontend.linha-nova.create');
    }

    public function linhaNova(Request $request, Notas $registro)
    {
        try{
            $input = $request->all();
            $input['tipo_bonus'] = BonusTipo::LINHA_NOVA;
            if(!empty($input['anexo_nf'])) $input['anexo_nf'] = Tools::fileUpload($input['anexo_nf'], 'linha-nova/');
            $nova_nota = $registro->create($input);

            //notifica gerente da nova criada
            $usuario = $nova_nota->cadastros->first();
            $gerente = User::where('email', '=', $usuario->gerente_automocao_email)
            ->orWhere('email', '=', $usuario->gerente_componentes_email)
            ->first();
            
            $this->enviaEmailGerente($gerente, $usuario, 'Linha Nova');

            return redirect()->route('notas-cadastradas')->with('success', 'Nota linha nova inserida com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Erro ao registrar nota:' .$e->getMessage()]);
        };
    }

    private function enviaEmailGerente($gerente, $usuario, $tipo_nota)
    {
        try{
            $send = Mail::send('emails.notifica-gerente', ['usuario' => $usuario, 'gerente' => $gerente, 'tipo_nota' => $tipo_nota], function($m) use ($usuario, $gerente, $tipo_nota)
             {
                 $m->to($gerente->email, config('app.name'))
                    ->subject('[NOTA FISCAL EMITIDA] '.config('app.name'));
             });
             if($send) return true;
        }catch(\Exception $e){
            return back()->withErrors(['Gerente associado inválido ou o mesmo não existe no sistema. Sugestão: Altere o cadastro informando um email de gerente existe.']);
        }
    }
   
}
