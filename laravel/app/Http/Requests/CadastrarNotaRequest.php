<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CadastrarNotaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'valor' => 'required',
            'numero_nf' => 'required',
            'cnpj_terceiro' => 'required|unique:notas',
            'razao_social' => 'required'
        ];
    }
}
