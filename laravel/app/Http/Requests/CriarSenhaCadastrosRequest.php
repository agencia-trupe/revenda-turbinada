<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CriarSenhaCadastrosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'senha' => 'required|min:3|required_with:confirmar_senha|same:confirmar_senha',
            'confirmar_senha' => 'required|min:6'
        ];
    }
}
