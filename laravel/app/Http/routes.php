<?php

Route::group(['middleware' => ['web']], function () {    
    Route::get('/', 'HomeController@index')->name('home');
    Route::post('aceite-de-cookies', 'HomeController@postCookies')->name('aceite-de-cookies.post');
    Route::post('login', 'CadastroAuth\AuthController@login')->name('login-cadastro');
    Route::get('login', 'CadastroAuth\AuthController@showLoginForm')->name('login-cadastro');
    Route::get('logout', 'CadastroAuth\AuthController@logout')->name('logout-cadastro');
    Route::post('criar-senha', 'CadastroAuth\AuthController@createPassword')->name('criar-senha');

    Route::get('esqueci-minha-senha', 'HomeController@esqueci')->name('cadastros.esqueci');
    Route::get('redefinicao-de-senha/{token}', 'CadastroAuth\PasswordController@showResetForm')->name('password-reset');
    Route::post('redefinicao-de-senha', 'CadastroAuth\PasswordController@sendResetLinkEmail')->name('cadastros.redefinicao');
    Route::post('redefinir-senha', 'CadastroAuth\PasswordController@reset')->name('cadastros.redefinir');

    Route::group([
        'middleware'    => ['auth.cadastros']
    ],function (){
        Route::get('cadastrar-notas', 'CadastrarNotaController@cadastrarNota')->name('cadastrar-notas');
        Route::get('extrato-de-bonus', 'ExtratoBonusController@index')->name('extrato-de-bonus');        

        Route::get('cadastrar-revenda', 'CadastrarNotaController@cadastrarRevenda')->name('cadastrar-revenda');
        Route::post('revenda', 'CadastrarNotaController@revenda')->name('revenda');
        Route::get('cadastrar-linha-nova', 'CadastrarNotaController@cadastrarLinhaNova')->name('cadastrar-linha-nova');
        Route::post('linha-nova', 'CadastrarNotaController@linhaNova')->name('linha-nova');

        Route::get('notas-cadastradas', 'CadastrarNotaController@index')->name('notas-cadastradas');
        Route::get('politica-de-privacidade', 'HomeController@politicaDePrivacidade')->name('politica-de-privacidade');
    });

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        Route::group(['middleware' => 'admin'], function() {
            /* GENERATED ROUTES */

            Route::resource('cadastros', 'CadastrosController');
            Route::resource('notas', 'NotasController');
            Route::resource('bonus', 'BonusController');
            Route::post('pesquisa-notas', 'FiltroPesquisaController@notas')->name('filtro-notas');
            Route::post('pesquisa-bonus', 'FiltroPesquisaController@bonus')->name('filtro-bonus');
            Route::get('reenviar-email-usuario/{id}', 'CadastrosController@reenviaEmailUsuario')->name('email-usuario');
            Route::get('atualiza-saldo/{id}', 'FiltroPesquisaController@atualizaSaldoBonus')->name('atualiza-saldo');
            
            Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);
            Route::resource('usuarios', 'UsuariosController');
            Route::resource('politica-de-privacidade', 'PoliticaDePrivacidadeController', ['only' => ['index', 'update']]);
            Route::get('aceite-de-cookies', 'AceiteDeCookiesController@index')->name('painel.aceite-de-cookies');

            Route::post('ckeditor-upload', 'PainelController@imageUpload');
            Route::post('order', 'PainelController@order');
            Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

            Route::get('generator', 'GeneratorController@index')->name('generator.index');
            Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
        });
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
