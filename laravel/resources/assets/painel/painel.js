import Clipboard from './modules/Clipboard.js';
import DataTables from './modules/DataTables.js';
import DatePicker from './modules/DatePicker.js';
import DeleteButton from './modules/DeleteButton.js';
import Filtro from './modules/Filtro.js';
import GeneratorFields from './modules/GeneratorFields.js';
import ImagesUpload from './modules/ImagesUpload.js';
import MonthPicker from './modules/MonthPicker.js';
import MultiSelect from './modules/MultiSelect.js';
import OrderImages from './modules/OrderImages.js';
import OrderTable from './modules/OrderTable.js';
import TextEditor from './modules/TextEditor.js';

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

Clipboard();
DataTables();
DatePicker();
DeleteButton();
Filtro();
GeneratorFields();
ImagesUpload();
MonthPicker();
MultiSelect();
OrderImages();
OrderTable();
TextEditor();

$('.cnpj').mask('00.000.000/0000-00', { reverse: true });
$('.bonus').mask('#.##0,00', { reverse: true });

$(document).ready(function() {
    var max_fields = 10;
    var wrapper = $("#addItemCarouselHome");
    var add_button = $(".add_field_button");

    var x = 0;
    $(add_button).click(function(e) {
        e.preventDefault();
        if (x < max_fields) {
            x++;
            $(wrapper).append('<div class="row addItemCarouselHome">' +
                '<div class="col-md-5">' +
                '<label>Descrição</label>' +
                '<input type="text" name="carrossel[' + x + '][descricao]" class="form-control">' +
                '</div>' +
                '<div class="col-md-5">' +
                '<label>Imagem</label>' +
                '<input type="file" name="carrossel[' + x + '][foto]" class="form-control">' +
                '</div>' +
                '<div class="col-md-2">' +
                '<button class="btn btn-danger remove_field" type="button">Remover</button>' +
                '</div>' +
                '</div>');
        }
    });

    $(wrapper).on("click", ".remove_field", function(e) {
        e.preventDefault();
        $(this).parents('.addItemCarouselHome').remove();
        x--;
    })
});