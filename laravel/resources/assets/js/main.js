import AjaxSetup from "./AjaxSetup";

AjaxSetup();

$('.cnpj').mask('00.000.000/0000-00', { reverse: true });
$('.reais').mask('#.##0,00', { reverse: true });
$('.numero_nf').mask('######', { reverse: true })

$(document).ready(function() {
    $('.close').click(function() {
        $('.cnpj-existe').hide();
    });

    // AVISO DE COOKIES
    $(".aviso-cookies").hide();

    if (window.location.href == routeHome) {
        $(".aviso-cookies").show();

        $(".aceitar-cookies").click(function() {
            var url = window.location.href + "aceite-de-cookies";

            $.ajax({
                type: "POST",
                url: url,
                success: function(data, textStatus, jqXHR) {
                    $(".aviso-cookies").hide();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR, textStatus, errorThrown);
                    $(".aviso-cookies").hide();
                },
            });
        });
    }
});

var file = document.querySelector('#anexo_nf');
file.addEventListener('change', (e) => {
    // Get the selected file
    var [file] = e.target.files;
    // Get the file name and size
    var { name: fileName, size } = file;
    // Convert size in bytes to kilo bytes
    var fileSize = (size / 1000).toFixed(2);
    // Set the text content
    var fileNameAndSize = `${fileName} - ${fileSize}KB`;
    document.querySelector('.file-name').textContent = fileNameAndSize;
});