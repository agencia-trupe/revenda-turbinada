<!DOCTYPE html>
<html>
<head>
    <title>[NOTA FISCAL CRIADA] {{ config('app.name') }}</title>
    <meta charset="utf-8">
</head>
<body>
    <p>- Olá {{ $gerente->name }}!</p>
    <p>O usuário <strong>[ {{ $usuario->nome }} ]</strong> criou uma nova Nota Fiscal to tipo <strong>[ {{ $tipo_nota }} ]</strong> e já está disponível para validação. </p>

    <span style='font-weight:bold;font-size:16px;'>Faça login em: >> <a href="{{ url('/painel/login') }}">Revenda Turbinada</a> << para visualizar</span><br>
</body>
</html>