<!DOCTYPE html>
<html>
<head>
    <title>[CONTATO] {{ config('app.name') }}</title>
    <meta charset="utf-8">
</head>
<body>
    <p>- Olá {{ $usuario->nome }}!</p>
    <p>Seu acesso está disponível. Acesse: >> <a href="{{ url('/login') }}">Login</a> <<</p>

    <span style='font-weight:bold;font-size:16px;'>Informe seu CNPJ: {{ $usuario->cnpj }} e crie uma senha.</span><br>
</body>
</html>