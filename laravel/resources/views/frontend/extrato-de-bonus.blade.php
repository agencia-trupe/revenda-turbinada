@extends('frontend.common.template')

@section('content')

<div class="container-fluid extrato-bonus pt-1 pb-3">
    <div class="row-grid">
        <div class="col-8 col-md-12 col-sm-12">
            <h1>Extrato de utilização de bônus</h1>
            @if (isset($registros))
            <p>
                <span class="alter-font">Sua empresa:</span> {{ \Auth::guard('cadastros')->user()->razao_social }}. | CNPJ {!! \Auth::guard('cadastros')->user()->cnpj !!}
            </p>
        
            <div class="limit-width">
                <table>
                    <thead class="text-left">
                        <th>Data da nota</th>
                        <th>Valor do bônus utilizado</th>
                    </thead>
                    <tbody>
                        @foreach ($registros as $registro)                            
                        <tr>
                            <td class="text-right">{!! date("d/m/Y", strtotime($registro->data)) !!}</td>
                            <td class="text-right"><strong>R$ {!! $registro->valor !!}</strong></td>
                            </tr>
                            @endforeach                
                        </tbody>
                    </table>
                    
                    <p>
                        <span class="alter-font">Total de bônus utilizado:</span> <span class="valor-utilizado">R$ {!! $bonus->total_bonus_utilizado ?: '0,00' !!}</span>
                    </p>
                    @else
                    <div class="info-warning">
                        <p>Nenhum bônus utilizado.</p>
                    </div>         
            @endif  

                <a href="{{ route('notas-cadastradas') }}" class="btn-geral">
                    <button type="button">Voltar</button>
                </a>
            </div>
        </div>
    </div>
</div>

@endsection