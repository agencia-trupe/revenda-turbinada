@extends('frontend.common.template')

@section('content')
<div class="container-fluid pt-1 ">
    @include('frontend.common.buttons')
</div>
@include('frontend.common.tipo-bonus')
<div class="container-fluid pb-3">
        @include('frontend.common.flash')
        <div class="row-grid">
            <div class="col-6 col-md-6 col-sm-12">
                
                {!! Form::open(['route' => 'linha-nova', 'class' => 'form-style', 'files'  => true]) !!}
                
                @include('frontend.linha-nova.form', ['submitText' => 'Salvar'])
                
                {!! Form::close() !!}
            </div>
        </div>
</div>

@endsection
