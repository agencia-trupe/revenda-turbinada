
<div class="row-grid">
    {!! Form::label('numero_nf', 'Nº Nota fiscal', ['class' => 'col-sm-2 col-form-label ']) !!}
    <div class="col-sm-10 p-0 input ">
    {!! Form::text('numero_nf', null, ['class' => 'form-control numero_nf']) !!}
    </div>
</div>

<div class="row-grid">
    {!! Form::label('valor', 'Valor', ['class' => 'col-sm-2 col-form-label ']) !!}
    <div class="col-sm-10 p-0 input ">
    {!! Form::text('valor', null, ['class' => 'form-control reais']) !!}
    </div>
</div>

<div class="row-grid">
    {!! Form::label('anexo_nf', 'Anexar NF', ['class' => 'col-sm-2 col-form-label ']) !!}
    <div class="col-sm-10 p-0 input ">
        <div class="file-input">
            {!! Form::file('anexo_nf', ['class' => 'form-control file']) !!}
            <label for="anexo_nf">
                Fazer upload do arquivo
                <p class="file-name"></p>
              </label>
        </div>
    </div>
</div>
<input type="hidden" name="usuario_id" value="{{ Auth::guard('cadastros')->user()->id }}">
<input type="hidden" name="razao_social" value="{{ Auth::guard('cadastros')->user()->razao_social }}">
<input type="hidden" name="cnpj_terceiro" value="{{ Auth::guard('cadastros')->user()->cnpj }}">

<div class="row-grid">
    <label for=""></label>
    <div class="col-sm-10 p-0 pt-2 btn-send ">
        {!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
        <a class="btn" href="{{ route('notas-cadastradas') }}">Cancelar </a>
    </div>
</div>


