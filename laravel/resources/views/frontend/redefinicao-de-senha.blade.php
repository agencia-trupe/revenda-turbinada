@extends('frontend.common.template')

@section('content')


<div class="container login">
    <div class="banner-login">
        <img class="home-foguete d-sm-none" src="{{ asset('assets/img/layout/img-home-foguete.png') }}" alt="">
        <div class="container-fluid">
            <div class="row-grid content">
                <div class="col-7 col-md-5 d-sm-none"></div>
                <div class="col-5 col-md-7 col-sm-12">
                    <div class="logo">
                        <img src="{{ asset('assets/img/layout/marca-metaltex.png') }}" alt="">
                    </div>
                        <img class="logo-dois" src="{{ asset('assets/img/layout/marca-revenda-turbinada-home.png') }}" alt="">
                        
                        <h3>Redefinição de Senha</h3>

                        @if(session('enviado'))
                            <div class="enviado">
                                {{ session('enviado') }}
                            </div>
                        @else
                            <form action="{{ route('cadastros.redefinir') }}" method="POST">
                                @if($errors->any())
                                    <div class="flash flash-erro">
                                        @foreach($errors->all() as $error)
                                        {{ $error }}<br>
                                        @endforeach
                                    </div>
                                @endif

                                <input type="hidden" name="token" value="{{ $token }}">
                                <input type="hidden" name="email" value="{{ $email }}" required>

                                {!! csrf_field() !!}
                                <p style="padding: 10px 0; font-size:10px;"><font color="red">Min. 6 caracter e 1 caracter especial</font></p>
                                <div class="form-row">
                                    <input type="password" name="password" placeholder="senha" required>
                                </div>
                                <div class="form-row">
                                    <input type="password" name="password_confirmation" placeholder="repetir senha" required>
                                </div>
                                <input type="submit" value="REDEFINIR SENHA">
                            </form>
                        @endif                
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
