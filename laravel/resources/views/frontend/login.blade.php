@extends('frontend.common.template')

@section('content')

<div class="container login">
    <div class="banner-login">
        <img class="home-foguete d-sm-none" src="{{ asset('assets/img/layout/img-home-foguete.png') }}" alt="">
        <div class="container-fluid">
            <div class="row-grid content">
                <div class="col-7 col-md-5 d-sm-none"></div>
                <div class="col-5 col-md-7 col-sm-12">
                    <div class="logo">
                        <img src="{{ asset('assets/img/layout/marca-metaltex.png') }}" alt="">
                    </div>
                        <img class="logo-dois" src="{{ asset('assets/img/layout/marca-revenda-turbinada-home.png') }}" alt="">
                        @include('frontend.common.flash')
                        @if (isset($cnpj))
                        <form action="{{ route('criar-senha') }}" method="POST">
                            {{ csrf_field() }}
                        <div class="criar-senha">
                            <h2>Primeiro acesso • Criar senha</h2>
                            <p>Seu CNPJ é: {!! $cnpj !!} - Crie sua senha:</p>
                            <div class="form-row">
                                <label for="">Senha</label>
                                <input type="password" name="senha" >
                            </div>
                            <div class="form-row">
                                <label for="">Repetir Senha</label>
                                <input type="password" name="confirmar_senha" >
                            </div>
                            <input type="hidden" name="cnpj" value="{!! $cnpj !!}">
                            <input type="submit" value="Entrar">
                        </div>
                    </form> 
                        @else
                        <form action="{{ route('login-cadastro') }}" method="POST">
                            {{ csrf_field() }}
                            <p style="padding: 10px 0; font-size:10px;"><font color="red">Caso seja o primeiro acesso, basta apenas inserir o CNPJ</font></p>  
                                <div class="form-row">
                                    <label for="">CNPJ</label>
                                    <input type="text" name="cnpj" class="cnpj" value="{{ old('cnpj') }}">
                                </div>
                                <div class="form-row">
                                    <label for="">Senha</label>
                                    <input type="password" name="senha" >
                                </div>
                                <input type="submit" value="Entrar">
                                <p class="esqueci-senha">
                                    <a href="{{ route('cadastros.esqueci') }}" ><em>Esqueci minha senha >></em></a>
                                </p>
                            @endif                            
                        </form>                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection