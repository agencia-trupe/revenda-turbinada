<?php 
use App\Models\BonusTipo;
?>
@extends('frontend.common.template')
@section('content')

<section class="container-fluid pt-1 pb-3">
    @include('frontend.common.buttons')
    @include('frontend.common.flash')
    <article class="row-grid table-content">
        <div class="col-12 col-md-12 col-sm-12">
            <h2>Suas notas cadastradas:</h2>

            <div class="table-responsive">

                <table>
                    <thead class="text-left">                    
                        <th>Tipo Bônus</th>
                        <th>Razão social | cnpj</th>
                        <th>nota fiscal nº</th>
                        <th>Soma valores <br>produtos MTX</th>
                        <th>Validação | Status</th>
                        <th>Valor do bônus</th>
                    </thead>
                    <tbody>
                        @if (isset($registros))
                            @foreach ($registros as $registro)                            
                            <tr>
                                <td>
                                    <?php $tipo_nome = BonusTipo::find($registro->tipo_bonus); ?>
                                    {{  ucfirst($tipo_nome['nome'])  }}
                                </td>
                                <td>{{ ucwords($registro->razao_social) }}.<br>{!! $registro->cnpj_terceiro !!}</td>
                                <td>
                                    @if ($registro->anexo_nf)
                                        @if ($registro->tipo_bonus == 1)
                                            <a target="_blank" class="anexo-nf" href="{{ url('assets/img/revenda/'. $registro->anexo_nf) }}">
                                        @else
                                            <a target="_blank" class="anexo-nf" href="{{ url('assets/img/linha-nova/'. $registro->anexo_nf) }}">
                                        @endif                                  
                                        {!! $registro->numero_nf !!} <img src="{{ asset('assets/img/layout/icone-arquivo.svg') }}" alt="">
                                    </a>
                                    @else
                                        {!! $registro->numero_nf !!} <img src="{{ asset('assets/img/layout/icone-arquivo.svg') }}" alt="">
                                    @endif
                                </td>
                                <td class="text-right">R$ {!! $registro->valor !!}</td>
                                <td class="status @if($registro->status == "aprovado") status-aprovado @endif @if($registro->status == "reprovado") status-reprovado @endif"><strong>{!! $registro->status ? $registro->status . ' • ' . date("d/m/Y", strtotime($registro->status_data)) : 'aguardando validação' !!}</strong></td>
                                <td class="text-right"><strong>R$ {!! $registro->bonus ?: '0,00' !!}</strong></td>
                            </tr>
                            @endforeach
                        @endif                    
                    </tbody>
                </table>
            </div>
        </div>
    </article>
</section>

@endsection