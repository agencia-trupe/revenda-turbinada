@extends('frontend.common.template')

@section('content')
<div class="container-fluid pt-1 ">
    @include('frontend.common.buttons')
</div>
@include('frontend.common.tipo-bonus')

@if ($errors->any('cnpj'))    
<div class="cnpj-existe">
    <div class="popup">
        <h2>O cadastro não pode ser completado</h2>
        <p>O CNPJ informado já foi inserido anteriormente em sua lista de Notas Fiscais de Revenda. Apenas uma Nota Fiscal de Revenda pode ser computada por Campanha.</p>
        <p>Revise os dados para corrigi-los ou cancele a inclusão desta nota fiscal.</p>
        <button type="button" class="close">OK</button>
    </div>
</div>
@endif

<div class="container-fluid pb-3 ">
        @include('frontend.common.flash')
        <div class="row-grid">
            <div class="col-6 col-md-6 col-sm-12">
                
                {!! Form::open(['route' => 'revenda', 'class' => 'form-style', 'files'  => true]) !!}
                
                @include('frontend.revenda.form', ['submitText' => 'Salvar'])
                
                {!! Form::close() !!}
            </div>
        </div>
</div>

@endsection
