<header class="container header-content pt-2 pb-2">
    <section class="container-fluid">
        <article class="row-grid ">
            <div class="col-4 col-md-4 col-sm-7 border-right pr-3">
                <img width="100%" src="{{ asset('assets/img/layout/marca-revenda-turbinada.png') }}" alt="">
            </div>
            <div class="col-4 col-md-4 col-sm-12 pl-3">
                <div class="info-usuario">
                    <h2>Olá!</h2>
                    <h1>{!! Auth::guard('cadastros')->user()->nome !!}</h1>
                    <a href="{{ route('logout-cadastro') }}"><p>[ SAIR <span><img src="{{ asset('assets/img/layout/x-fechar.svg') }}" alt=""></span>]</p></a>
                </div>
            </div>
            @if (isset($bonus->usuario_id))
            <?php if(isset($saldo->saldo) != 0) { $bonus->total_bonus = $saldo->saldo; } ?>
                <div class="col-4 col-md-4 col-sm-12 pl-5">
                    <div class="valores-float">
                            <div class="container-valores">
                                <h2>Seu total de bônus:</h2>
                                <p class="saldo-bonus">R$ {!! $bonus->total_bonus ?: '0,00' !!}</p>
                            
                                <p>Revenda: <span class="valor">R$ {!! $bonus->total_bonus_revenda ?: '0,00' !!}</span></p>
                                <p>Linha nova: <span class="valor">R$ {!! $bonus->total_bonus_linha_nova ?: '0,00' !!}</span></p>
                                <hr>
                                <p>Bônus utilizado: <span class="valor-utilizado">{!! $bonus->total_bonus_utilizado ?: '0,00' !!}</span></p>
                            </div>
                    </div>
                </div>
            @endif
        </article>
    </section>
</header>
