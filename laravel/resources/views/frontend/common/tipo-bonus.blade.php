<div class="container-fluid cadastrar-content">
    <article class="row-grid">
        <div class="col-12 col-sm-12 col-md-12">
            <h1>Cadastrar nova nota fiscal</h1>
        </div>
            <div class="col-6 col-md-6 col-sm-12 pb-0 pt-0">    
                <p>Tipo de Bônus: 
                    <a href="{{ route('cadastrar-revenda') }}">
                        <button @if(Tools::routeIs('cadastrar-revenda')) class="active" @endif>Revenda</button>
                    </a> 
                    <a href="{{ route('cadastrar-linha-nova') }}">
                        <button @if(Tools::routeIs('cadastrar-linha-nova')) class="active" @endif>Linha nova</button>
                    </a>
                </p>
            </div>
        </article>
</div>