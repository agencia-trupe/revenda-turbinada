@if (session('error')){{ dd(session('error')) }}
                                <div class="error-login">
                                    <p>{{ session('error') }}</p>
                                </div>
                            @endif
                        @if($errors->any())
                            <div class="error-login">
                                @foreach($errors->all() as $error)
                                {!! $error !!}<br>
                                @endforeach
                            </div>
                        @endif

                        @if (session('success'))
                            <div class="flash flash-sucesso">
                                {{ session('success') }}
                            </div>
                        @endif