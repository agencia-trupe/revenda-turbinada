@extends('frontend.common.template')

@section('content')

    <div class="container login">
        <div class="banner-login">
            <img class="home-foguete d-sm-none" src="{{ asset('assets/img/layout/img-home-foguete.png') }}" alt="">
            <div class="container-fluid">
                <div class="row-grid content">
                    <div class="col-7 col-md-5 d-sm-none"></div>
                    <div class="col-5 col-md-7 col-sm-12">
                        <div class="logo">
                            <img src="{{ asset('assets/img/layout/marca-metaltex.png') }}" alt="">
                        </div>
                            <img class="logo-dois" src="{{ asset('assets/img/layout/marca-revenda-turbinada-home.png') }}" alt="">
                            
                            <h3>Esqueci Minha Senha</h3>

                            @if(session('enviado'))
                                <div class="enviado">
                                    {{ session('enviado') }}
                                </div>
                            @else
                                <form action="{{ route('cadastros.redefinicao') }}" method="POST">
                                    @if($errors->any())
                                        <div class="erro">
                                            @foreach($errors->all() as $error)
                                            {{ $error }}<br>
                                            @endforeach
                                        </div>
                                    @endif

                                    {!! csrf_field() !!}
                                    <div class="form-row">

                                        <input type="email" style="width: 100%" name="email" value="{{ old('email') }}" placeholder="e-mail" required>
                                    </div>
                                    <input type="submit" value="REDEFINIR SENHA">
                                </form>
                            @endif                
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
