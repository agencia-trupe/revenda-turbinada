@extends('frontend.common.template')

@section('content')

<section class="container-fluid pt-1 pb-3">
    @include('frontend.common.buttons')

    @include('frontend.common.tipo-bonus')
</section>

@endsection