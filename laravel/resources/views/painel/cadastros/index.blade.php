@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Clientes             
            <a href="{{ route('painel.cadastros.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Usuário</a>
        </h2>
    </legend>

    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Razão Social</th>
                <th>E-mail</th>
                <th>CNPJ</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        @if (!isset($usuarios))
        <div class="alert alert-warning" role="alert">
            Nenhum registro.
          </div>
        @else
        <tbody>
            @foreach ($usuarios as $usuario)
    
                <tr class="tr-row">                    
                    <td>{{ $usuario->razao_social }}</td>
                    <td>{{ $usuario->email }}</td>
                    <td>{{ $usuario->cnpj }}</td>
                    <td style="width: 40%" class="crud-actions text-right">
                        {!! Form::open([
                            'route'  => ['painel.cadastros.destroy', $usuario->id],
                            'method' => 'delete'
                        ]) !!}
    
                        <div class="btn-group ">
                            <a href="{{ route('email-usuario', $usuario->id) }}" class="btn btn-info btn-sm">
                                <span class="glyphicon glyphicon-envelope" style="margin-right:10px;"></span> Reenviar e-mail
                            </a>
                            <a href="{{ route('painel.cadastros.edit', $usuario->id ) }}" class="btn btn-primary btn-sm ">
                                <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                            </a>
                            
                            <a href="{{ route('painel.bonus.edit', $usuario->id ) }}" class="btn btn-success btn-sm ">
                                <span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Bônus Utilizado
                            </a>
    
                            @if($usuario->id)
                                <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                            @endif
                        </div>
    
                        {!! Form::close() !!}
                    </td>
                </tr>
    
            @endforeach
        </tbody>
        @endif
    </table>

@endsection
