@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Editar Cliente</h2>
    </legend>

    {!! Form::model($usuario, [
        'route' => ['painel.cadastros.update', $usuario->id],
        'method' => 'patch'])
    !!}

    @include('painel.cadastros.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
