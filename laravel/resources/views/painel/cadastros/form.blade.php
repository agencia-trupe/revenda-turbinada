@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('razao_social', 'Razão social') !!}
    {!! Form::text('razao_social', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cnpj', 'CNPJ') !!}
    {!! Form::text('cnpj', null, ['class' => 'form-control cnpj']) !!}
</div>

<div class="form-group">
  {!! Form::label('gerente_automocao_email', 'Gerente Automoção') !!}
  {!! Form::email('gerente_automocao_email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
  {!! Form::label('gerente_componentes_email', 'Gerente Componentes') !!}
  {!! Form::email('gerente_componentes_email', null, ['class' => 'form-control']) !!}
</div>
<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.cadastros.index') }}" class="btn btn-default btn-voltar">Voltar</a>
