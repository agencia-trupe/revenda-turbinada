<ul class="nav navbar-nav">
    <li class="dropdown @if(Tools::routeIs('painel.cadastros*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Clientes
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.cadastros.index')) class="active" @endif>
                <a href="{{ route('painel.cadastros.index') }}">Listar</a>
            </li>
        </ul>
    </li>

    <li class="dropdown @if(Tools::routeIs('painel.notas*')) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Notas
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.notas.index')) class="active" @endif>
                <a href="{{ route('painel.notas.index') }}">Listar</a>
            </li>
        </ul>
    </li>

    @if (\Auth::user()->tipo == "administracao")  
        <li class="dropdown @if(Tools::routeIs('painel.bonus*')) active @endif">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                Bônus utilizado
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
                <li @if(Tools::routeIs('painel.bonus.index')) class="active" @endif>
                    <a href="{{ route('painel.bonus.index') }}">Clientes</a>
                </li>
            </ul>
        </li>
    @endif

    <!--<li @if(Tools::routeIs('painel.notas*')) class="active" @endif>
        <a href="#">Notas</a>
    </li>-->
</ul>