<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <form action="{{ route('filtro-bonus') }}" method="post">                
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group row">
                        <div class="col-lg-3 pl-0">
                            <input type="text" name="cnpj" class="form-control cnpj" value="{{ old('cnpj') }}" placeholder="Cliente/Distribuidor">
                        </div>
                        
                        <input type="hidden" name="rota" value="{!! $rota !!}">
                <div class="col-lg-2 ">
                    <input type="submit" value="Pesquisar" class="btn btn-info">
                </div>
                </div>
            </form>
        </div>
    </div>
</div>