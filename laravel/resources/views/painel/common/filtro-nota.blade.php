<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <form action="{{ route('filtro-notas') }}" method="post">                
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group row">
                        <div class="col-lg-3 pl-0">                        
                            <input type="number" name="numero_nf" class="form-control" value="{{ old('numero_nf') }}" placeholder="Nº Nota Fiscal">
                        </div>
                        <div class="col-lg-3 p-0 ">
                            <input type="text" name="razao_social" class="form-control" value="{{ old('razao_social') }}" placeholder="Razão Social">
                        </div>
                        <input type="hidden" name="rota" value="{!! $rota !!}">
                <div class="col-lg-2 ">
                    <input type="submit" value="Pesquisar" class="btn btn-info">
                </div>
                </div>
            </form>
        </div>
    </div>
</div>