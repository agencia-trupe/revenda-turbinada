<?php 
use App\Models\BonusTipo;
?>
@include('painel.common.flash')

<p>Cliente/Distribuidor: <strong>{{ ucwords($nota->razao_social) }} | {{ $nota->cnpj_terceiro }}</strong></p>
<hr>

<div class="form-group row">
    <div class="col-sm-6">
        {!! Form::label('cpnj_terceiro', 'CNPJ') !!}
        {!! Form::text('cnpj_terceiro', null, ['class' => 'form-control', 'disabled']) !!}
    </div>
    <div class="col-sm-6">
        {!! Form::label('razao_social', 'Razão social') !!}
        {!! Form::text('razao_social', null, ['class' => 'form-control', 'disabled']) !!}
    </div>
</div>

<div class="form-group">
    @if (isset($nota->anexo_nf))
       <button type="button" data-toggle="modal" data-target="#notaFiscal" class="btn btn-info">Visualizar Nota</button>
    @endif
</div>

<div class="form-group row">
    <div class="col-sm-6">
        {!! Form::label('numero_nf', 'Número Nota Fiscal') !!}
        {!! Form::text('numero_nf', null, ['class' => 'form-control', 'disabled']) !!}
    </div>
    <div class="col-sm-6">
        {!! Form::label('valor', 'Valor') !!}
        {!! Form::text('valor', null, ['class' => 'form-control', 'disabled']) !!}
    </div>
</div>


<div class="form-group">
    <?php $tipo_nome = BonusTipo::find($nota->tipo_bonus); ?>
    <label>Tipo bônus: <span style="color:red">{{ ucfirst($tipo_nome->nome) }}</span></label>
</div>

<hr>

<div class="form-group row">
    <div class="col-sm-6 col-lg-3 col-md-3 col-6">
    {!! Form::label('bonus', 'Aplicar bônus da nota') !!}
    {!! Form::text('bonus', null, ['class' => 'form-control bonus']) !!}
    </div>
    <div class="col-sm-6 col-lg-3 col-md-3 col-6">
        {!! Form::label('status', 'Status') !!}
        {!! Form::select('status', ['aprovado' => 'Aprovar', 'reprovado' => 'Reprovar'], null, ['placeholder' => 'Selecione', 'class' => 'form-control']) !!}
    </div>
</div>
<hr>


{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.notas.index') }}" class="btn btn-default btn-voltar">Voltar</a>

<!-- Modal Image-->
<div class="modal fade" id="notaFiscal" tabindex="-1" aria-labelledby="notaFiscal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Nota Fiscal</h5>
          <button style="margin-top: -25px;" type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body text-center">
            <?php $extension = substr($nota->anexo_nf, -3, 3); ?>
            @if ($nota->tipo_bonus == BonusTipo::REVENDA)
                @if ($extension == 'pdf')
                <iframe src="{{ asset('assets/img/revenda/'. $nota->anexo_nf) }}" width="100%" height="600px"></iframe>
                @else                    
                    <img width="100%" src="{{ asset('assets/img/revenda/'. $nota->anexo_nf) }}" />
                @endif
            @endif
            @if ($nota->tipo_bonus == BonusTipo::LINHA_NOVA)
                @if ($extension == 'pdf')
                    <iframe src="{{ asset('assets/img/linha-nova/'. $nota->anexo_nf) }}" width="100" height="600px"></iframe>
                @else                    
                    <img width="100%" src="{{ asset('assets/img/linha-nova/'. $nota->anexo_nf) }}" />
                @endif
            @endif 
        </div>
      </div>
    </div>
  </div>
