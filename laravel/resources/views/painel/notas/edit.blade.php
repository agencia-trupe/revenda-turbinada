@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Validar notas</h2>
    </legend>
    
    {!! Form::model($nota, [
        'route' => ['painel.notas.update', $nota->id],
        'method' => 'patch'])
    !!}

    @include('painel.notas.form', ['submitText' => 'Atualizar'])

    {!! Form::close() !!}

@endsection
