<?php 
use App\Models\BonusTipo;
?>
@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Notas Recebidas
        </h2>
    </legend>

    @include('painel.common.filtro-nota', [
        'rota' => 'painel.notas.index',
    ])

    <hr>    
    <table class="table table-striped table-bordered table-hover ">
        <thead>
            <tr>
                <th>Cliente/Distribuidor</th>
                <th>Tipo</th>
                <th>Razão da NF</th>
                <th>Valor NF</th>
                <th>Valor Bônus</th>
                <th>Status</th>
                <th>Data</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>
        @if (isset($pesquisa_registro))
            <!-- verifica se a busca retornou erro -->
            @if (is_string($pesquisa_registro))
                <div class="alert alert-warning" role="alert">
                    {!! $pesquisa_registro !!}
                </div>
            @else                
                <tbody>
                    @foreach ($pesquisa_registro as $nota)
                        <tr class="tr-row">
                            <td>{!! $nota->cadastros->razao_social !!}</td>
                            <td> <?php $tipo_nome = BonusTipo::find($nota->tipo_bonus); ?>
                                {!! ucwords($tipo_nome->nome) !!}
                            </td>
                            <td>
                                @if ($nota->tipo_bonus == BonusTipo::REVENDA)                                    
                                    {{ $nota->razao_social }}
                                @else
                                    -
                                @endif
                            </td>
                            <td>R$ {{ $nota->valor }}</td>
                            <td>R$ {!! $nota->bonus ?: '-' !!}</td>
                            <td @if ($nota->status == "aprovado") class="status-aprovado" @endif 
                                @if ($nota->status == "reprovado") class="status-reprovado" @else class="status-aguardando" @endif>
                                {{ ucwords($nota->status) ?: ucwords('Aguardando aprovação') }}
                            </td>
                            <td>{{ date('d/m/Y', strtotime($nota->created_at)) }}</td>

                            <td class="crud-actions">
                                <a style="margin-bottom: 5px" href="{{ route('painel.notas.edit', $nota->id ) }}" class="btn btn-primary btn-sm btn-block ">
                                    <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Validar
                                </a>    
                                {!! Form::open([
                                    'route'  => ['painel.notas.destroy', $nota->id],
                                    'method' => 'delete'
                                ]) !!}
                                    @if($nota->id)
                                        <button type="submit" class="btn btn-danger btn-sm btn-block btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                                    @endif
                                {!! Form::close() !!}    
                            </td>
                        </tr> 
                        @endforeach
                </tbody>
            @endif
        @else            
            @if (isset($notas))
            <tbody>
                @foreach ($notas as $nota)    
                <tr class="tr-row">
                    <td>{!! $nota->cadastros->razao_social !!}</td>
                    <td> <?php $tipo_nome = BonusTipo::find($nota->tipo_bonus); ?>
                        {!! ucwords($tipo_nome->nome) !!}
                    </td>
                    <td>
                        @if ($nota->tipo_bonus == BonusTipo::REVENDA)                                    
                            {{ $nota->razao_social }}
                        @else
                            -
                        @endif
                    </td>
                    <td>R$ {{ $nota->valor }}</td>
                    <td class="text-right">{!! $nota->bonus ? 'R$ '. $nota->bonus : '-' !!}</td>
                    <td @if ($nota->status == "aprovado") class="status-aprovado" @endif 
                        @if ($nota->status == "reprovado") class="status-reprovado" @else class="status-aguardando" @endif>
                        {{ ucwords($nota->status) ?: ucwords('Aguardando aprovação') }}
                    </td>
                    <td>{{ date('d/m/Y', strtotime($nota->created_at)) }} </td>

                    <td class="crud-actions">
                        <a style="margin-bottom: 5px" href="{{ route('painel.notas.edit', $nota->id ) }}" class="btn btn-primary btn-sm btn-block ">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Validar
                        </a>    
                        {!! Form::open([
                            'route'  => ['painel.notas.destroy', $nota->id],
                            'method' => 'delete'
                        ]) !!}
                            @if($nota->id)
                                <button type="submit" class="btn btn-danger btn-sm btn-block btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                            @endif
                        {!! Form::close() !!}    
                    </td>
                </tr> 
                @endforeach
            </tbody>
            @else
            <div class="alert alert-warning" role="alert">
                Nenhuma nota registrada.
            </div>
            @endif
        @endif
    </table>

@endsection
