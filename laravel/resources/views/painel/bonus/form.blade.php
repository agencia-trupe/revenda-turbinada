<?php 
use App\Models\BonusTipo;
?>
@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control', 'disabled']) !!}
</div>

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::email('email', null, ['class' => 'form-control', 'disabled']) !!}
</div>

<div class="form-group">
    {!! Form::label('razao_social', 'Razão social') !!}
    {!! Form::text('razao_social', null, ['class' => 'form-control', 'disabled']) !!}
</div>

<div class="form-group">
    {!! Form::label('cnpj', 'CNPJ') !!}
    {!! Form::text('cnpj', null, ['class' => 'form-control cnpj', 'disabled']) !!}
</div>

<div class="form-group">
  {!! Form::label('gerente_automocao_email', 'Gerente Automoção') !!}
  {!! Form::email('gerente_automocao_email', null, ['class' => 'form-control', 'disabled']) !!}
</div>

<div class="form-group">
  {!! Form::label('gerente_componentes_email', 'Gerente Componentes') !!}
  {!! Form::email('gerente_componentes_email', null, ['class' => 'form-control', 'disabled']) !!}
</div>

<input type="hidden" name="usuario_id" value="{{ $usuario->id }}">

<hr>

<div class="form-group row">
    <div class="col-sm-12 col-lg-3 col-md-3 col-12">
    {!! Form::label('valor_bonus', 'Bônus utilizado') !!}
    {!! Form::text('valor_bonus', null, ['class' => 'form-control bonus']) !!}
    </div>
    <div class="col-sm-12 col-lg-3 col-md-3 col-12">
      {!! Form::label('numero_nf', 'Número NF') !!}
      {!! Form::text('numero_nf', null, ['class' => 'form-control']) !!}
      </div>
      <div class="col-sm-12 col-lg-3 col-md-3 col-12">
        {!! Form::label('numero_nf', 'Data') !!}
        <input style="line-height: 22px" type="date" name="data" class="form-control">
        </div>
</div>



{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.bonus.index') }}" class="btn btn-default btn-voltar">Voltar</a>
