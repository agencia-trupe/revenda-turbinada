@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Inserir Bônus Utilizado</h2>
    </legend>
    
    {!! Form::model($usuario, [
        'route' => ['painel.bonus.create', $usuario->id],
        'method' => 'patch'])
    !!}

    @include('painel.bonus.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
