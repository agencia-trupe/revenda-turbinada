<?php 
use App\Models\BonusTipo;
?>
@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Bônus Utilizado
        </h2>
    </legend>

    @include('painel.common.filtro-bonus', [
        'rota' => 'painel.bonus.index',
    ])
    <hr>

    <table class="table table-striped table-bordered table-hover ">
        <thead>
            <tr>
                <th>Cliente/Distribuidor</th>
                <th>Número NF</th>
                <th>Bônus Utilizado</th>
                <th>Data</th>
                <th>Saldo</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>
        @if (isset($pesquisa_registro))
            <tbody>
                @foreach ($pesquisa_registro as $nota)    
                    <tr class="tr-row">
                        <td>{{ $nota->cadastros->cnpj }}</td>
                        <td>{{ $nota->numero_nf }}</td>
                        <td>R$ {{ $nota->valor }}</td>
                        <td>{{ date("d/m/Y", strtotime($nota->data)) }}</td>
                        <td>R$ {{ $nota->valor }}</td>
                        <td class="crud-actions">
                            {!! Form::open([
                                'route'  => ['painel.bonus.destroy', $nota->id],
                                'method' => 'delete'
                            ]) !!}
                                @if($nota->id)
                                    <button type="submit" class="btn btn-danger btn-sm btn-block btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                                @endif
                            {!! Form::close() !!}    
                        </td>
                    </tr>    
                @endforeach
            </tbody>
        @else
            @if (isset($bonus_utilizado))
            <tbody>
                @foreach ($bonus_utilizado as $nota)    
                    <tr class="tr-row">
                        <td>{{ $nota->cadastros->cnpj }}</td>
                        <td>{{ $nota->numero_nf }}</td>
                        <td>R$ {{ $nota->valor }}</td>
                        <td>{{ date("d/m/Y", strtotime($nota->data)) }}</td>
                        <td>R$ {{ $nota->saldo }}</td>
                        <td class="crud-actions">
                            <a style="margin-bottom: 5px" href="{{ route('atualiza-saldo', $nota->usuario_id ) }}" class="btn btn-success btn-sm btn-block ">
                                <span class="glyphicon glyphicon-refresh" style="margin-right:10px;"></span>Atualizar saldo
                            </a>
                            {!! Form::open([
                                'route'  => ['painel.bonus.destroy', $nota->id],
                                'method' => 'delete'
                            ]) !!}
                                @if($nota->id)
                                    <button type="submit" class="btn btn-danger btn-sm btn-block btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                                @endif
                            {!! Form::close() !!}    
                        </td>
                    </tr>    
                @endforeach
            </tbody>
            @else
            <div class="alert alert-warning" role="alert">
                Nenhum bônus registrado.
            </div>
            @endif
        @endif
    </table>

@endsection
